/**
 * Dependencies
 * @ignore
 */
import './assets/css/styles.css'

/**
 * Expose module exports to browser window
 */
import * as main from './main'
Object.assign(window, main)
