/**
 * Constants
 * @ignore
 */
export const availableToppings = [
  {
    name: 'Cheese',
    price: 30,
  },
  {
    name: 'Mushroom',
    price: 50,
  },
  {
    name: 'Pepperoni',
    price: 75,
  },
  {
    name: 'Pineapple',
    price: 35,
  },
]

/**
 * Pizza Builder
 *
 * @description
 * New Builder Class using the Module Design Pattern
 *
 * @param {string} appId The name of the element to mount
 * @returns {PizzaBuilder}
 */
export default function PizzaBuilder(appId) {
  // References to reused DOM Elements.
  const elApp = document.getElementById(appId)
  const elPizza = elApp.children
    .namedItem('pizza-container')
    .children.namedItem('pizza')
  const elToppings = elApp.children.namedItem('toppings')
  const elToppingActions = elToppings.children.namedItem('topping--actions')
  const elOrder = elApp.children.namedItem('order')
  const elOrderTotal = document.getElementById('order--total')
  const elOrderToppings = document.getElementById('order--toppings')

  // Verify child elements.
  if (!elPizza || !elToppings || !elOrder) {
    throw new Error(
      "Please ensure you have elements with id's 'pizza', 'toppings' and 'order' inside your app element."
    )
  }

  // A list of toppings currently on the pizza.
  let currentToppings = []

  // Helper Objects - IIFE Design Pattern
  const orderManager = (function () {
    let total = 0

    return {
      update() {
        total = 80
        currentToppings.forEach((topping) => {
          total += topping.price
        })

        elOrderTotal.innerText = total.toFixed(2)

        this.updateToppingList()
      },

      updateToppingList() {
        elOrderToppings.innerHTML = ''

        if (currentToppings.length > 0) {
          currentToppings.forEach((topping) => {
            const elTopping = document.createElement('li')
            elTopping.innerText =
              topping.name + ' - ' + topping.price.toFixed(2)
            elOrderToppings.appendChild(elTopping)
          })
        } else {
          elOrderToppings.innerHTML = '<li>No toppings added</li>'
        }
      },
    }
  })()

  const toppingManager = (function () {
    return {
      hasTopping(topping) {
        return currentToppings.find(
          (_topping) => _topping.name === topping.name
        )
      },

      add(topping) {
        currentToppings.push(topping)
        const elTopping = document.createElement('div')
        elTopping.className = `topping topping--${topping.name.toLowerCase()}`
        elPizza.appendChild(elTopping)
      },

      remove(topping) {
        currentToppings = currentToppings.filter(
          (_topping) => _topping.name !== topping.name
        )
        document
          .querySelector(`.topping--${topping.name.toLowerCase()}`)
          .remove()
      },
    }
  })()

  return {
    /**
     * Create a basic Pizza Base
     * @returns {Builder}
     */
    createBase() {
      const base = document.createElement('div')
      base.className = 'pizza-base'
      elPizza.appendChild(base)
      orderManager.update()
      return this
    },

    /**
     * Create the actions for all the toppings in the Topping list.
     * @returns {Builder}
     */
    createToppingActions() {
      availableToppings.forEach((topping) => {
        // Use descructering to extract the topping's name and price from the current topping.
        const { price, name } = topping
        // Programmatically create a button
        const toppingButton = document.createElement('button')
        toppingButton.className = 'btn btn-topping'
        toppingButton.id = name
        toppingButton.innerText = name
        toppingButton.dataset.price = price
        // Programmatically add a onlick listener and link it to the toggleTopping function.
        // Remember to take into account execution context ;)
        toppingButton.onclick = () => {
          this.toggleTopping(topping)
        }
        // Append the button the elToppingsActions element reference.
        elToppingActions.appendChild(toppingButton)
      })
      return this
    },

    /**
     * Add or Remove a topping.
     * Uses the toppingManager object.
     * @param topping
     */
    toggleTopping(topping) {
      if (toppingManager.hasTopping(topping)) {
        toppingManager.remove(topping)
      } else {
        toppingManager.add(topping)
      }

      orderManager.update()
    },
  }
}
