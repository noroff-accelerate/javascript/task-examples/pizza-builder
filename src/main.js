/**
 * Dependencies
 * @ignore
 */
import PizzaBuilder from "./builder"

/**
 * App
 * @ignore
 */
const builder = new PizzaBuilder("app")
builder.createBase().createToppingActions()

/**
 * Exports
 * @ignore
 */
export { builder }
